import java.awt.*;
import java.util.Vector;

public class CatAndMouseWorld implements RLWorld{
	public int bx, by;

	public int mx, my;
	public int[] cx, cy;
	public int[] chx, chy;
	public int hx, hy;
	public boolean gotCheese = false;
	
	public int catscore = 0, mousescore = 0;
	public int cheeseReward=100, deathPenalty=20, hitWallPenalty = 10, noMovePenalty = 10, dumbPenalty = 10;
	public int penalty = 0;
	
	// Modif Akbar
	Vector<Integer> playX, playY, trainX, trainY;
	// End Modif
	
	//modif vai
	public int NUM_CAT = 1, NUM_CHEESE = 1, NUM_OBJECTS=6;
	
	/* EDITED BY ALDI DOANTA KURNIA */
	/* START EDITING*/
	public int REMAINING_CHEESE = 1;
	/* FINISH EDITING*/
	
	public int POSSIBLE_STATES = 4; //keju, kucing, tembok, kosong
	public static final int EMPTY = 0, CAT = 1, CHEESE = 2, WALL = 3; 
	
	public int NUM_STATES = 4; //default
	
	
	public static final int GO_FORWARD = 0, TURN_LEFT = 1, TURN_RIGHT = 2;//, GO = 3, ON = 4;
	public enum MouseOrientation{N, W, E, S, NW, SW, NE, SE};
	//NW N NE
	// W - E
	//SW S SE
	public MouseOrientation currentOrientation = MouseOrientation.N, predictedOrientation = MouseOrientation.N;
	MouseOrientation[] lastOrientation;
	int[] lastX;
	int[] lastY;
	int recordedOrientation = 6;
	int counter = 0;
	
	static final int NUM_ACTIONS=3, WALL_TRIALS=100;
	static final double INIT_VALS=0;
		
	int[] stateArray;
	double waitingReward;
	public boolean[][] walls;

	// Modif Akbar
	int mode; // 0=play, 1=train
	// End Modif
	
	public void setNumStates(int _n){
		NUM_STATES = _n;
	}
	
	public CatAndMouseWorld(int x, int y, int numWalls, int newMode, Vector<Integer> playX2, Vector<Integer> playY2, Vector<Integer> trainX2, Vector<Integer> trainY2) {
		bx = x;
		by = y;
		makeWalls(x,y,numWalls);
		mode = newMode;
		
		playX = playX2;
		playY = playY2;
		trainX = trainX2;
		trainY = trainY2;
		
		resetState();
	}
	
	public CatAndMouseWorld(int x, int y, boolean[][] newwalls, int newMode, Vector<Integer> playX2, Vector<Integer> playY2, Vector<Integer> trainX2, Vector<Integer> trainY2) {
		bx = x;
		by = y;
		
		walls = newwalls;
		mode = newMode;
		
		playX = playX2;
		playY = playY2;
		trainX = trainX2;
		trainY = trainY2;
		
		resetState();
	}

	/******* RLWorld interface functions ***********/
	public int[] getDimension() { 
		int[] retDim = new int[NUM_STATES+2];
		//int[] retDim = new int[NUM_OBJECTS+1];
		int i;
		for (i=0; i<NUM_STATES;) {
		//for (i=0; i<NUM_OBJECTS;) {
			retDim[i++] = POSSIBLE_STATES;
			//retDim[i++] = NUM_STATES/2;
			//retDim[i++] = bx;
			//retDim[i++] = by;
		}
		
		//for (int j = i; j < NUM_STATES; j++){
			//retDim[j] = NUM_STATES;
		//}
		
		//retDim[NUM_STATES] = NUM_ACTIONS;
		//retDim[i] = 8;
		retDim[i] = POSSIBLE_STATES;
		retDim[i+1] = NUM_ACTIONS;
		//retDim[i+1] = 9;
		//retDim[i+2] = 9;
		return retDim;
	}
		
	// given action determine next state
	public int[] getNextState(int action) {
		// action is mouse action:  0=u 1=ur 2=r 3=dr ... 7=ul
		Dimension d = getCoords(action);
		int ax=d.width, ay=d.height;
		if (legal(ax,ay)) {
			// move agent
			//System.out.println(currentOrientation + " " + action + " " + predictedOrientation);
			
			if (mx == ax && my == ay) penalty += noMovePenalty;
			/* EDITED BY ALDI DOANTA KURNIA */
			/* START EDITING*/
			else mousescore--; //caused by moving one step
			/* FINISH EDITING*/
			/*
			lastX[counter] = mx;
			lastY[counter] = my;
			counter = (counter+1)%recordedOrientation;
			if ((lastX[0] == lastX[1] && lastY[0] == lastY[1]) && (lastX[1] == lastX[2] && lastY[1] == lastY[2]) && (lastX[2] == lastX[3] && lastY[2] == lastY[3])) penalty += 10;
			*/
			//int[] temp = getState();
			//for (int i = 0; i < temp.length; i++){
				//if (temp[i] == WALL) penalty += 5;
			//}
			mx = ax; my = ay;
			/*
			lastOrientation[counter] = currentOrientation;
			counter = (counter+1)%recordedOrientation;
			for (int i = 0; i < recordedOrientation-2; i++){
				if (lastOrientation[i] == lastOrientation[i+2]) penalty+= dumbPenalty;
			}
			*/
			
			currentOrientation = predictedOrientation;
		} else {
		
			/* EDITED BY ALDI DOANTA KURNIA */
			/* START EDITING*/
			mousescore = mousescore - 2; //caused by hitting the wall
			/* FINISH EDITING*/
		
			penalty += hitWallPenalty;
			//System.err.println("Illegal action: "+action);
		}
		// update world
		//moveCat();
		waitingReward = calcReward(action);
		
		//should this still exist?
		// if mouse has cheese, relocate cheese
		/*
		if ((mx==chx[0]) && (my==chy[0])) {
			d = getRandomPos();
			chx[0] = d.width;
			chy[0] = d.height;
		}
		*/
		/*// if cat has mouse, relocate mouse
		if ((mx==cx) && (my==cy)) {
			d = getRandomPos();
			mx = d.width;
			my = d.height;
		}*/
		/* EDITED BY ALDI DOANTA KURNIA */
		/* START EDITING*/
		for (int i = 0; i < NUM_CHEESE; i++) {
			if ((chx[i]==mx) && (chy[i]==my)) REMAINING_CHEESE--;
		}
		/* FINISH EDITING*/
		
		return getState();
	}
	
	public double getReward(int i) { return getReward(); }
	public double getReward() {	return waitingReward; }
	
	public boolean validAction(int action) {
		//Dimension d = getCoords(action);
		//return legal(d.width, d.height);
		return true;
	}
	
	Dimension getCoords(int action) {
	//NW N NE
	// W - E
	//SW S SE
	//{{7,0,1},
	//{6,0,2},
	//{5,4,3}};
		int ax=mx, ay=my;
		switch(action) {
			//case GO:
			//case ON:
			case GO_FORWARD: 
					switch (currentOrientation){
						case N:
							ay = my - 1;
							break;
						case W:
							ax = mx - 1;
							break;
						case E:
							ax = mx + 1;
							break;
						case S:
							ay = my + 1;
							break;
						case NW:
							ay = my - 1;
							ax = mx - 1;
							break;
						case SW:
							ay = my + 1;
							ax = mx - 1;
							break;
						case NE:
							ay = my - 1 ;
							ax = mx + 1;
							break;
						case SE:
							ay = my + 1;
							ax = mx + 1;
							break;
					}
					break;
			case TURN_LEFT:
					switch (currentOrientation){
						case N:
							predictedOrientation = MouseOrientation.NW;
							break;
						case W:
							predictedOrientation = MouseOrientation.SW;
							break;
						case E:
							predictedOrientation = MouseOrientation.NE;
							break;
						case S:
							predictedOrientation = MouseOrientation.SE;
							break;
						case NW:
							predictedOrientation = MouseOrientation.W;
							break;
						case SW:
							predictedOrientation = MouseOrientation.S;
							break;
						case NE:
							predictedOrientation = MouseOrientation.N;
							break;
						case SE:
							predictedOrientation = MouseOrientation.E;
							break;
					}
					break;
			case TURN_RIGHT: 
					switch (currentOrientation){
						case N:
							predictedOrientation = MouseOrientation.NE;
							break;
						case W:
							predictedOrientation = MouseOrientation.NW;
							break;
						case E:
							predictedOrientation = MouseOrientation.SE;
							break;
						case S:
							predictedOrientation = MouseOrientation.SW;
							break;
						case NW:
							predictedOrientation = MouseOrientation.N;
							break;
						case SW:
							predictedOrientation = MouseOrientation.W;
							break;
						case NE:
							predictedOrientation = MouseOrientation.E;
							break;
						case SE:
							predictedOrientation = MouseOrientation.S;
							break;
					}
					break;
			/*
			case 3: ay = my + 1; ax = mx + 1; break;
			case 4: ay = my + 1; break;
			case 5: ay = my + 1; ax = mx - 1; break;
			case 6: ax = mx - 1; break;
			case 7: ay = my - 1; ax = mx - 1; break;
			*/
			default: //System.err.println("Invalid action: "+action);
		}
		return new Dimension(ax, ay);
	}

	// find action value given x,y=0,+-1
	int getAction(int x, int y) {
		int[][] vals={{TURN_LEFT,GO_FORWARD,TURN_RIGHT},
		              {TURN_LEFT,GO_FORWARD,TURN_RIGHT},
					  {TURN_LEFT,TURN_LEFT,TURN_RIGHT}};
		if ((x<-1) || (x>1) || (y<-1) || (y>1) || ((y==0)&&(x==0))) return -1;
		int retVal = vals[y+1][x+1];
		return retVal;
	}

	public boolean endState() { return endGame(); }
	public int[] resetState() { 
		//modif vai
		chx = new int[NUM_CHEESE];
		chy = new int[NUM_CHEESE];
		cx = new int[NUM_CAT];
		cy = new int[NUM_CAT];
		//end modif
		/*DOANTA/*
		/* START EDITING*/
		REMAINING_CHEESE = NUM_CHEESE;
		/* FINISH EDITING*/
		lastOrientation = new MouseOrientation[recordedOrientation];
		lastX = new int[recordedOrientation];
		lastY = new int[recordedOrientation];
		
		catscore = 0;
		mousescore = 0;
		setRandomPos(); 
		return getState();
	}
		
	public double getInitValues() { return INIT_VALS; }
	/******* end RLWorld functions **********/
	
	public int[] getState() {
		// translates current state into int array
		stateArray = new int[NUM_STATES+1];
		//stateArray = new int[NUM_OBJECTS];
		//modif vai
		/*
		int cheeseCounter = 0, catCounter = 0;
		for (int i = 0; i < NUM_OBJECTS;){
			//System.out.println(i);
			if (i < 1){
				//System.out.println("i = " + i + " mx= " + mx);
				stateArray[i++] = mx;
				//System.out.println("i = " + i + " mx= " + my);
				stateArray[i++] = my;
			} else if (i > 1 && i < (2 + 2*NUM_CAT)){
				stateArray[i++] = cx[catCounter];
				//System.out.println("i = " + (i-1) + " cx= " + cx[catCounter] + " catCounter" + catCounter);
				stateArray[i++] = cy[catCounter];
				//System.out.println("i = " + (i-1) + " cx= " + cy[catCounter] + " catCounter" + catCounter);
				catCounter++;
			} else if (i >= (2 + 2*NUM_CAT)){
				stateArray[i++] = chx[cheeseCounter];
				//System.out.println("i = " + (i-1) + " chx= " + chx[cheeseCounter] + " catCounter" + cheeseCounter);
				stateArray[i++] = chy[cheeseCounter];
				//System.out.println("i = " + (i-1) + " chx= " + chy[cheeseCounter] + " catCounter" + cheeseCounter);
				cheeseCounter++;
			}
			//System.out.println(i);
		}
		*/
		
		boolean continueLoop = false;
		/*
		switch (currentOrientation){
			case N:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== mx) && (cy[j]== (my - i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== mx) && (chy[j]== (my - i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx, my - i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case W:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx-i, my)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case E:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx + i,my)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case S:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx, my + i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case NW:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx-i, my - i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case SW:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx - i, my + i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case NE:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx+i, my - i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case SE:
				for (int i = 1; i <= NUM_STATES/2; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (!continueLoop) continue;
					if (legal(mx + i, my + i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
		}
		
		for (int i = 0; i < NUM_STATES; i++){
			stateArray[i] = ;
		}
		
		switch (currentOrientation){
			case N:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== mx) && (cy[j]== (my - i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== mx) && (chy[j]== (my - i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx, my - i)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
			case W:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx-i, my)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
			case E:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx + i,my)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
			case S:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx, my + i)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
			case NW:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx-i, my - i)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
			case SW:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx - i, my + i)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
			case NE:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx+i, my - i)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
			case SE:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							stateArray[i] = i-1;
							i++;
							continueLoop = true;
							break;
						}
					}
					if (!continueLoop) continue;
					if (legal(mx + i, my + i)){ //wall
						stateArray[i-1] = WALL;
						stateArray[i] = i-1;
						i++;
					} else {
						stateArray[i-1] = EMPTY;
						stateArray[i] = i-1;
						i++;
					}
					
				}
				break;
		}
		*/
		
		switch (currentOrientation){
			case N:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== mx) && (cy[j]== (my - i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== mx) && (chy[j]== (my - i))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx, my - i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case W:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx-i, my)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case E:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx + i,my)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case S:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx, my + i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case NW:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my-i))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx-i, my - i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case SW:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx-i)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx-i)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx - i, my + i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case NE:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my-i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my-i))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue;
					if (!legal(mx+i, my - i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
			case SE:
				for (int i = 1; i <= NUM_STATES; i++){
					continueLoop = false;
					for (int j = 0; j < NUM_CAT; j++){
						if ((cx[j]== (mx+i)) && (cy[j]== (my+i))) {
							stateArray[i-1] = CAT;
							continueLoop = true;
							break;
						}
					}
					if (continueLoop) continue; 
					for (int j = 0; j < NUM_CHEESE; j++){
						if ((chx[j]== (mx+i)) && (chy[j]== (my+i))) {
							stateArray[i-1] = CHEESE;
							continueLoop = true;
							break;
						}
					}
					if (!continueLoop) continue;
					if (legal(mx + i, my + i)){ //wall
						stateArray[i-1] = WALL;
					} else {
						stateArray[i-1] = EMPTY;
					}
					
				}
				break;
		}
		
		for (int j = 0; j < NUM_CAT; j++){
			if ((cx[j]== (mx)) && (cy[j]== (my))) {
				stateArray[NUM_STATES] = CAT;
				return stateArray;
			}
		}
		for (int j = 0; j < NUM_CHEESE; j++){
			if ((chx[j]== (mx)) && (chy[j]== (my))) {
				stateArray[NUM_STATES] = CHEESE;
				return stateArray;
			}
		}
		if (legal(mx, my)){ //wall
			stateArray[NUM_STATES] = WALL;
			return stateArray;
		} else {
			stateArray[NUM_STATES] = EMPTY;
			return stateArray;
		}
		
		/*
		switch(currentOrientation){
			case N:
				stateArray[NUM_STATES] = 0;
				break;
			case NE:
				stateArray[NUM_STATES] = 1;
				break;
			case NW:
				stateArray[NUM_STATES] = 2;
				break;
			case SE:
				stateArray[NUM_STATES] = 3;
				break;
			case SW:
				stateArray[NUM_STATES] = 4;
				break;
			case E:
				stateArray[NUM_STATES] = 5;
				break;
			case W:
				stateArray[NUM_STATES] = 6;
				break;
			case S:
				stateArray[NUM_STATES] = 7;
				break;
		}
		*/
		//stateArray[NUM_STATES] = currentOrientation.hashCode();
		//stateArray[NUM_STATES+1] = my;
		
		//end modif
		//return stateArray;
	}

	public double calcReward(int action) {
		double newReward = 0;
		for (int i = 0; i < NUM_CHEESE; i++){
			if ((mx==chx[i])&&(my==chy[i])) {
				
				/* EDITED BY ALDI DOANTA KURNIA */
				/* START EDITING*/
				//mousescore++;
				//replaced by code below
				mousescore += (bx + by); //caused by acquiring a cheese
				/* FINISH EDITING*/
			
				newReward += cheeseReward;
				//System.out.println("yeay");
			}
		}
		
		for (int i = 0; i < NUM_CAT; i++){
			if ((cx[i]==mx) && (cy[i]==my)) {
				catscore++;
				
				/* EDITED BY ALDI DOANTA KURNIA */
				/* START EDITING*/
				mousescore -= (bx + by); //caused by getting caught by a cat
				/* FINISH EDITING*/
				
				newReward -= deathPenalty;
				//System.out.println("woo");
			}
		}
		/*
		Dimension d = getCoords(action);
		int ax=d.width, ay=d.height;
		if (!legal(ax, ay)){
			newReward -= hitWallPenalty;
		}
		*/
		//System.out.println(newReward);
		//System.out.println(penalty);
		newReward -= penalty;
		penalty = 0;
		//newReward -= p;
		//if ((mx==hx)&&(my==hy)&&(gotCheese)) newReward += 100;
		//System.out.println(newReward);
		return newReward;		
	}
	
	public void setRandomPos() {
		//modif vai
		Dimension d;
		for (int i = 0; i < NUM_CAT; i++){
			d = getRandomPos();
			cx[i] = d.width;
			cy[i] = d.height;
		}
		
		d = getRandomPos();
		mx = d.width;
		my = d.height;
		
		for (int i = 0; i < NUM_CHEESE; i++){
			d = getRandomPos();
			chx[i] = d.width;
			chy[i] = d.height;
		}
		//end modif
		d = getRandomPos();
		hx = d.width;
		hy = d.height;
	}

	boolean legal(int x, int y) {
		return ((x>=0) && (x<bx) && (y>=0) && (y<by)) && (!walls[x][y]);
	}

	boolean endGame() {
		//return (((mx==hx)&&(my==hy)&& gotCheese) || ((cx==mx) && (cy==my)));
		for (int i = 0; i < NUM_CAT; i++) {
			if ((cx[i]==mx) && (cy[i]==my)) return true;
		}
		if(REMAINING_CHEESE == 0) return true;
		return false;
	}

	// Modif Akbar
	Dimension getRandomPos() {
		int i;
		if (mode==0) {
			do {
				i = (int)(Math.random() * playX.size());
			}
			while (!legal(playX.get(i),playY.get(i)));
			return new Dimension(playX.get(i),playY.get(i));
		}
		else if (mode==1) {
			do {
				i = (int)(Math.random() * trainX.size());
			}
			while (!legal(trainX.get(i),trainY.get(i)));
			return new Dimension(trainX.get(i),trainY.get(i));
		}
		return null;
	}
	
	Dimension getRandomPosWall() {
		int nx, ny;
		nx = (int)(Math.random() * bx);
		ny = (int)(Math.random() * by);
		for(int trials=0; (!legal(nx,ny)) && (trials < WALL_TRIALS); trials++){
			nx = (int)(Math.random() * bx);
			ny = (int)(Math.random() * by);
		}
		return new Dimension(nx, ny);
	}
	// End Modif
	
	/******** heuristic functions ***********/
	Dimension getNewPos(int x, int y, int tx, int ty) {
		int ax=x, ay=y;
		
		if (tx==x) ax = x;
 		else ax += (tx - x)/Math.abs(tx-x); // +/- 1 or 0
		if (ty==y) ay = y;
 		else ay += (ty - y)/Math.abs(ty-y); // +/- 1 or 0
		
		// check if move legal	
		if (legal(ax, ay)) return new Dimension(ax, ay);
		
		// not legal, make random move
		while(true) {
			// will definitely exit if 0,0
			ax=x; ay=y;
			ax += 1-(int) (Math.random()*3);
			ay += 1-(int) (Math.random()*3);
			
			//System.out.println("old:"+x+","+y+" try:"+ax+","+ay);
			if (legal(ax,ay)) return new Dimension(ax,ay);
		}
	}

	//placeholders, these two methods shoudnt exist
	void moveCat() {
		Dimension newPos = getNewPos(cx[0], cy[0], mx, my);
		cx[0] = newPos.width;
		cy[0] = newPos.height;		
	}

	void moveMouse() {
		Dimension newPos = getNewPos(mx, my, chx[0], chy[0]);
		mx = newPos.width;
		my = newPos.height;
	}
	
	//?
	int mouseAction() {
		Dimension newPos = getNewPos(mx, my, chx[0], chy[0]);
		return getAction(newPos.width-mx,newPos.height-my);
	}
	/******** end heuristic functions ***********/


	/******** wall generating functions **********/
	void makeWalls(int xdim, int ydim, int numWalls) {
		walls = new boolean[xdim][ydim];
		
		// loop until a valid wall set is found
		for(int t=0; t<WALL_TRIALS; t++) {
			// clear walls
			for (int i=0; i<walls.length; i++) {
				for (int j=0; j<walls[0].length; j++) walls[i][j] = false;
			}
			
			float xmid = xdim/(float)2;
			float ymid = ydim/(float)2;
			
			// randomly assign walls.  
			for (int i=0; i<numWalls; i++) {
				Dimension d = getRandomPos();
				
				// encourage walls to be in center
				double dx2 = Math.pow(xmid - d.width,2);
				double dy2 = Math.pow(ymid - d.height,2);
				double dropperc = Math.sqrt((dx2+dy2) / (xmid*xmid + ymid*ymid));
				if (Math.random() < dropperc) {
					// reject this wall
					i--;
					continue;
				}
				
				
				walls[d.width][d.height] = true;
			}
			
			// check no trapped points
			if (validWallSet(walls)) break;
			
		}
		
	}
	
	//modif vai
	//should be static?
	public boolean[][] retWalls(int xdim, int ydim, int numWalls) {
		boolean[][] retwalls = new boolean[xdim][ydim];
		
		// loop until a valid wall set is found
		for(int t=0; t<WALL_TRIALS; t++) {
			// clear walls
			for (int i=0; i<retwalls.length; i++) {
				for (int j=0; j<retwalls[0].length; j++) retwalls[i][j] = false;
			}
			
			float xmid = xdim/(float)2;
			float ymid = ydim/(float)2;
			
			// randomly assign walls.  
			for (int i=0; i<numWalls; i++) {
				Dimension d = getRandomPos();
				
				// encourage walls to be in center
				double dx2 = Math.pow(xmid - d.width,2);
				double dy2 = Math.pow(ymid - d.height,2);
				double dropperc = Math.sqrt((dx2+dy2) / (xmid*xmid + ymid*ymid));
				if (Math.random() < dropperc) {
					// reject this wall
					i--;
					continue;
				}
				
				
				retwalls[d.width][d.height] = true;
			}
			
			// check no trapped points
			if (validWallSet(retwalls)) break;
			
		}
		return retwalls;
		
	}
	
	public void setNumObjects(int cheeseNum, int catNum){
		NUM_CAT = catNum;
		NUM_CHEESE = cheeseNum;
		NUM_OBJECTS = 2 + catNum*2 + cheeseNum*2;
		resetState();
	}
	//end modif
	
	
	boolean validWallSet(boolean[][] w) {
		// copy array
		boolean[][] c;
		c = new boolean[w.length][w[0].length];
		
		for (int i=0; i<w.length; i++) {
			for (int j=0; j<w[0].length; j++) c[i][j] = w[i][j];
		}
		
		// fill all 8-connected neighbours of the first empty
		// square.
		boolean found = false;
		search: for (int i=0; i<c.length; i++) {
			for (int j=0; j<c[0].length; j++) {
				if (!c[i][j]) {
					// found empty square, fill neighbours
					fillNeighbours(c, i, j);
					found = true;
					break search;
				}
			}
		}
		
		if (!found) return false;
		
		// check if any empty squares remain
		for (int i=0; i<c.length; i++) {
			for (int j=0; j<c[0].length; j++) if (!c[i][j]) return false;
		}
		return true;
	}
	
	void fillNeighbours(boolean[][] c, int x, int y) {
		c[x][y] = true;
		for (int i=x-1; i<=x+1; i++) {
			for (int j=y-1; j<=y+1; j++)
				if ((i>=0) && (i<c.length) && (j>=0) && (j<c[0].length) && (!c[i][j])) 
					fillNeighbours(c,i,j);
		}
	}
	/******** wall generating functions **********/

}
